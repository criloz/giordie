Design
------

the app consist

- web server for show the surveys, stats, etc.

- a numbers of script for automate the reading of the email from the imap server,
  parse them to  a json document and store in the database (CouchDB, http://couchdb.apache.org/ ),
  also register the new transactions on getvero (http://www.getvero.com/) and triggers the events.

- each user is identify by a uuid generated in the moment of parse a new email

- these scripts run using cron jobs


Technology
----------

The app will be written in pure Python, and using django web framework.


Libraries
---------

In addition to Python 2.7, the following Python libraries will be required
in order to run the application:

===================     =
**CouchDB 1.0**          CouchDb python api
**Tornado 2.0**          web framework
**Djago 0.9.14**         web framework

===================     =

These libraries are all known to work on Windows, Linux, and OSX platforms.
Some changes or additions may be required during the development process.
All other required libraries are included with Python.