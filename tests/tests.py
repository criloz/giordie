from db import DB, EmailDB
import unittest
import re
from co_email.parser import EmailParser

class TestCase(unittest.TestCase):

    def setUp(self):
        DB.initialize()
        pass

    def test_parser(self):
        msg = """========= SECURITY STATEMENT ==========
It is not recommended that you ship product(s) or otherwise grant services relying solely upon this e-mail receipt.

========= GENERAL INFORMATION =========

Merchant : Gemm-Internet Access (652389)
Date/Time : 17-Oct-2012 7:46:40 MDT

========= ORDER INFORMATION =========
Invoice :
Description : Crown Paradise PV Internet Access - 1 Day - USD $16.00
Amount : 16.00 (USD)
Payment Method : Visa
Type : Authorization and Capture

============== RESULTS ==============
Response : This transaction has been approved.
Authorization Code : 01770A
ETransaction ID : 4720987081
Address Verification : AVS Not Applicable

==== CUSTOMER BILLING INFORMATION ===
Customer ID : 4199
First Name : William
Last Name : Spencer
Company :
Address :
City :
State/Province :
Zip/Postal Code :
Country :
Phone : 480-907-6946
Fax :
E-Mail : wjspencer11@gmail.com

==== CUSTOMER SHIPPING INFORMATION ===
First Name :
Last Name :
Company :
Address :
City :
State/Province :
Zip/Postal Code :
Country :

======= ADDITIONAL INFORMATION ======
Tax :
Duty :
Freight :
Tax Exempt :
PO Number :

========== MERCHANT DEFINED =========
p : cc"""
        parser = EmailParser()
        doc = parser.parse_message(msg)
        doctest = {'ORDER INFORMATION': {'Payment Method': 'Visa', 'Description': 'Crown Paradise PV Internet Access - 1 Day - USD $16.00', 'Hotel': 'Crown Paradise PV', 'Amount': '16.00 (USD)', 'Plan': '1 Day', 'Type': 'Authorization and Capture'}, 'MERCHANT DEFINED': {'p': 'cc'}, 'SECURITY STATEMENT': {}, 'RESULTS': {'Address Verification': 'AVS Not Applicable', 'Authorization Code': '01770A', 'Response': 'This transaction has been approved.', 'ETransaction ID': '4720987081'}, 'GENERAL INFORMATION': {'Merchant': 'Gemm-Internet Access (652389)', 'Date/Time': '17-Oct-2012 7:46:40 MDT'}, 'CUSTOMER BILLING INFORMATION': {'Last Name': 'Spencer', 'First Name': 'William', 'Customer ID': '4199', 'E-Mail': 'wjspencer11@gmail.com', 'Phone': '480-907-6946'}, 'CUSTOMER SHIPPING INFORMATION': {}, 'ADDITIONAL INFORMATION': {}}
        self.assertEqual(doctest,doc,"parser is not working correctly")

    def test_check_emails(self):
        emails = EmailDB.read_emails_by(EmailDB.Unread,"tester2")
        #check if there exist emails in the db
        self.assertNotEqual(len(emails),0,"No emails found in the db, please run some of this script  (load_all_emails.py or load_new_emails.py) first")
        #check if the email message have the right structure
        for i in emails:
            self.assertEqual(i["value"].keys(),['ORDER INFORMATION', 'MERCHANT DEFINED', 'SECURITY STATEMENT', 'RESULTS', 'GENERAL INFORMATION', 'CUSTOMER BILLING INFORMATION', 'message', '_id', 'CUSTOMER SHIPPING INFORMATION', 'ADDITIONAL INFORMATION'],"Found email with a bad structure")
            #check if exist the email of the costumer
            self.assertFalse(re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", i["value"]['CUSTOMER BILLING INFORMATION']["E-Mail"]) is None, "the email %s have bad format"%(i["value"]['CUSTOMER BILLING INFORMATION']["E-Mail"]))


if __name__ == '__main__':
    unittest.main()