'''
Created on Oct 13, 2012

@author: cristian
'''

import uuid
import hashlib 
import re
from db.base_db import DB,DBError
from db.views import *
import logging
from couchdb.client import  ViewResults

"""
================================================================================================
                Couchdb views(map/reduce functions) for obtain the emails
================================================================================================
"""
class GetResponses(Views):
    
    """ get all the  response for all the surveys for each costumers in a dictionary
        example {'First_Name': 'mahsheed', 'Last_Name': 'khajavi', 'surveys': [{'name': 'survey1', 'questions': {'comment': 'ok', 'q1': 'true', 'q2': 'true'}}, {'name': 'survey2', 'questions': {'TripAdvisorUser': 'dasdasdasd', 'service_rating': '1', 'review': 'asdasdasdasdasdasd', 'tip': 'sadasdsad', 'trip_date_month_year': '3,2012', 'ReviewTitle': 'sadsadasdas', 'sort_of_trip': 'Couples', 'overall_rating': '3'}}], 'email': 'mkhajav@gmail.com', 'hotel': 'Oasis Sens'}
    """
    map       = """
            function (doc) {
            
                  function getObjectLength(o) {
                      var length = 0;
            
                      for (var i in o) {
                          if (Object.prototype.hasOwnProperty.call(o, i)) {
                              length++;
                          }
                      }
                      return length;
                  }
                  var i;
                  var j;
                  if (doc.surveys) {
                      if (getObjectLength(doc.surveys) > 0) {
                          var user = {};
                          var transaction;
                          var k;
                          for (k in doc.transactions) {
                              transaction = doc.transactions[k];
                              break;
                          }
                          user.First_Name = transaction["CUSTOMER BILLING INFORMATION"]["First Name"];
                          user.Last_Name  = transaction["CUSTOMER BILLING INFORMATION"]["Last Name"];
                          user.email      = transaction["CUSTOMER BILLING INFORMATION"]["E-Mail"];
                          user.hotel      = transaction["ORDER INFORMATION"]["Hotel"];
                          user.surveys    = [];
                          for (i in doc.surveys) {
                              var survey = {};
                              survey.name = i;
                              survey.questions = {};
                              for (j in doc.surveys[i]) {
                                  survey.questions[j] = doc.surveys[i][j];
                              }
                              user.surveys.push(survey);
                          }
            
                          emit(null, user);
                      }
            
                  }
            
              }
                """



class UnreadDuplicates(Views):
    """ get all the duplicates transaction on the database which have not been read by a specific user"""
    check_var = "read_duplicates_by_%s"
    map       = """
                function(doc) {
                      var i;
                      if (doc.read_duplicates_by_%s===true){
                        return;
                      }
                      Object.size = function(obj) {
                                  var size = 0, key;
                                for (key in obj) {
                                    if (obj.hasOwnProperty(key)) size++;
                                }
                                return size;
                        };
                        
                      // Get the size of an object
                      var size = Object.size(doc.transactions);
                      if(size<2){
                          return;
                      }
                      for(i in doc.transactions){
                      emit(doc.uuid,doc.transactions[i]);
                      }
                }
                """

class Unread(Views):
    """ get all the non-duplicates transaction on the database which have not been read by a specific user"""
    check_var = "read_by_%s"
    map       =  """
                function(doc) {
                  var i;
                  if (doc.read_by_%s===true){
                    return;
                  }
                  for(i in doc.transactions){
                  emit(doc.uuid,doc.transactions[i]);
                  return;
                  }
                }
                """

class Events(Views):
    """ count the numbers of time that the events has been triggered """
    map       =  """
                function(doc) {
                  var i;
                  for(i in doc.events){
                      emit(i,doc.events[i]);
                  }
                }
                """
    reduce   = """
                function(keys, values, rereduce) {
                    return sum(values);
                }
               """
                
class Hotels(Views):
    """ obtain all the email registered in the database """
    map       = """
                function(doc) {
                      var i;
                      for(i in doc.transactions){
                          emit(doc.transactions[i]["ORDER INFORMATION"]["Hotel"],null);
                      }
                }
                """
class TransUUID(Views):
    """ view for allow select the email by transaction UUID"""
    map       =  """
                function(doc) {
                    emit(doc.uuid,null);
                }
                """              
class EmailDBError(DBError):
    """ email db base exception"""
    pass



"""
================================================================================================
               Email db class
================================================================================================
"""
class EmailDB(DB):
    """
        Static class for manage the email in couchdb
        have general methods for access to the views write below 
    """
    
    # views 
    UnreadDuplicates = UnreadDuplicates
    Unread           = Unread
    Hotels           = Hotels
    dbname           = "emails"
    UUID             = TransUUID
    Events           = Events
    log              = logging.getLogger("db.EmailDB")
    GetResponses     = GetResponses
    
    @classmethod
    def write(cls,emailDict,original_message):
        """ create a new document (email) on the DB """
        assert type(emailDict) == dict
        assert type(original_message) == str
        
        db = cls.get_db()
        db_doc =  db.get(emailDict["_id"],None)
        
        if db_doc == None:
            #create new email in local db
            db_doc = {}
            db_doc["transactions"] = {}
            #add unique id
            db_doc["uuid"] = str(uuid.uuid1()).replace("-","")
            
        m = hashlib.md5()
        m.update(original_message)
        
        db_doc["transactions"][m.hexdigest()] = emailDict
        
        db[emailDict["_id"]] = db_doc
    
    @classmethod
    def read_emails_by(cls,view,by):
        """read email for a specific user using a view(UnreadDuplicates,Unread), example EmailDB.read_emails_by(EmailDB.UnreadDuplicates, "user")"""
        assert by!=None
        assert issubclass(view, Views)
        
        db = cls.get_db()
        
        if (len(re.findall("[^a-zA-Z0-9]",by))):
            raise EmailDBError
        #create custom view
        _map = view.map%by
        
        #select document "_design/get"
        doc =  db.get("_design/get",{"views":{}})
        
        viewName = "{name}_by_{by}".format(name=view.__name__,by=by)
        
        #save view if not exist
        doc["views"][viewName]  = {"map":_map}
        
        db["_design/get"] = doc
        
        return db.view("get/"+viewName)
    
    @classmethod
    def get_hotels(cls):
        """ obtain all the hotels register in the database"""
        #TODO: use a reduce function 
        db = cls.get_db()
        
        view = cls.Hotels
        
        design_name = "get"
        #select document "_design/get_by"
        doc =  db.get("_design/"+design_name,{"views":{}})
        
        viewName = "{name}".format(name=view.__name__)
        
        #save view if not exist
        doc["views"][viewName]  = {"map":view.map}
        
        db["_design/"+design_name] = doc
        
        return db.view(design_name+"/"+viewName)
        
    @classmethod
    def get_email_by(cls,view,*args,**kargs):
        """get emails using some of this views (UUID)"""
        assert issubclass(view, Views)
        
        db = cls.get_db()
        
        design_name = "get_by"
        #select document "_design/get_by"
        doc =  db.get("_design/"+design_name,{"views":{}})
        
        viewName = "{name}".format(name=view.__name__)
        
        #save view if not exist
        doc["views"][viewName]  = {"map":view.map}
        
        db["_design/"+design_name] = doc
        
        return db.view(design_name+"/"+viewName,*args,**kargs)
    
    @classmethod
    def update(cls,docs):
        """ update  a emails documents"""
        assert type(docs) == list
        db = cls.get_db()
        db.update(docs)
    
    @classmethod
    def get_events_count(cls):
        """ get stats of all the event register by the surveys"""
        db = cls.get_db()
        
        view = cls.Events
        
        design_name = "get"
        #select document "_design/get_by"
        doc =  db.get("_design/"+design_name,{"views":{}})
        
        viewName = "{name}".format(name=view.__name__)
        
        #save view if not exist
        doc["views"][viewName]  = {"map":view.map,"reduce":view.reduce}
        
        db["_design/"+design_name] = doc
        _dict = {}
        for i in db.view(design_name+"/"+viewName,group_level=1):
            _dict[i["key"]] = i["value"]
        return _dict
        
        
    @classmethod
    def mark_emails_as_read_by(cls,emails,view,by):
        """check  email as read for a user"""
        assert issubclass(emails.__class__,ViewResults)
        assert by!=None
        assert issubclass(view, Views)
        db = cls.get_db()
        
        for i in emails:
            doc  = db[i["id"]]
            doc[view.check_var%by] = True
            db[i["id"]] = doc

if __name__=="__main__":
    DB.initialize()
    for i in   EmailDB.get_email_by(EmailDB.GetResponses):
        print i["value"]
