""" automate the  manage of getvero interface"""

import mechanize
import logging

class LoginError(Exception):
    """exception that is raised when was not possible login into getvero webpage account"""
    pass

class CsvImportError(Exception):
    """exception that is raised when occurs a error importing the csv file into getvero"""
    pass

class GetVeroWeb(object):
    """ getvero class for create new user on getvero using the webpage ui (currently the api can not do this)"""
    
    sign_in_page = "https://www.getvero.com/users/sign_in"
    logged_pages  = "https://www.getvero.com/connect","https://www.getvero.com/events?logged_in=true"
    
    use_tor      = False
    TOR_PROXY    = {"http" : "127.0.0.1:8118", "https" : "127.0.0.1:8118"}
    
    
    def __init__(self,user,password):
        """constructor"""
        #create logger
        self.log = logging.getLogger(self.__class__.__name__)
        
        self.__user__     = user
        self.__password__ = password
        #start mechinize browser
        self.__browser__  =  mechanize.Browser()
        self.__browser__.set_handle_robots(False)
  
    
    def import_users_form_csv(self,file,email_column):
        """ import user from a csv file see getvero interface"""
        
        self.log.info("Starting csv importing process")
        
        self.__browser__.open("http://www.getvero.com/people/csv/import")
        self.__browser__.select_form(nr=0)
        
        self.log.info("uploading file")
        
        self.__browser__.add_file(file["content"], 'text/plain', file["name"])
        self.__browser__.submit()
        
        #select the email column
        self.__browser__.select_form(nr=0)
        self.__browser__["mappings[%s]"%email_column] = ["email"]
        
        #import all the user in the csv file
        response = self.__browser__.submit()
        
        #chek if all its ok
        
        text =  response.read()
        
        if "People created" in text  or "Vero will begin importing your CSV file shortly" in text:
            self.log.info("Users has been imported")
        else:
            self.log.error("Could not import the csv file to getvero")
            raise CsvImportError("Could not import the csv file to getvero")
   
    
    def login(self):
        """ login into getvero page"""
        self.log.info("Starting loggin process")
        self.__browser__.open(self.sign_in_page)
        self.__browser__.select_form(nr=0)
        self.__browser__["user[email]"] = self.__user__
        self.__browser__["user[password]"] = self.__password__
        self.__browser__.submit()
        
        if not self.__browser__.geturl() in self.logged_pages:
            self.log.error("Error while try to login, check password and user, if the issue continue connect with a developer")
            raise LoginError("Error while try to login")
        self.log.info("logged into getvero")
        
        
if __name__ == "__main__":
    """ do some testing"""
    logging.basicConfig(level=logging.NOTSET)
    a = GetVeroWeb("test@test.com","tester")
    a.login()
    a.import_users_form_csv("filepath","E-Mail")