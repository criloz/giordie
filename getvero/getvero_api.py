'''
Created on Oct 12, 2012

@author: Cristian Lozano <criscalovis@gmail.com>
'''
import json
import requests

class GetVeroException(Exception):
    """Getvero api base exception"""
    pass

class GetVero(object):
    """ getvero api base class"""

    def __init__(self,aunth_token,dev_mode):
        '''
        Constructor
        '''
        self.__development_mode__ = dev_mode
        self.__aunth_token__      = aunth_token
         
    def put(self,url,data):
        """ do a put request to the getvero api"""
        data["development_mode"] =  self.__development_mode__    
        response = requests.put(url,data = json.dumps(data))
        return response
    
    def post(self,url,data):
        """ do a post request to the getvero api"""
        data["development_mode"] =  self.__development_mode__
        response = requests.post(url,data = json.dumps(data))
        return response

class GetVeroUsers(GetVero):
    """ 
        getvero users api
        see https://github.com/getvero/vero-api/blob/master/sections/api/users.md
    """
    __user_edit_url__        = "https://www.getvero.com/api/v2/users/edit.json"
    __user_edit_tag_url__    = "https://www.getvero.com/api/v2/users/tags/edit.json"
    __unsubscribe_user_url__ = "https://www.getvero.com/api/v2/users/unsubscribe.json"
    
    def __init__(self,aunth_token,dev_mode=True):
        '''
        Constructor
        '''
        GetVero.__init__(self,aunth_token,dev_mode)
   
    
    
    def edit_user(self,user,changes={}):
        """ see getvero documentation"""
        assert type(changes)==dict
        data = {
                "auth_token": self.__aunth_token__,
                "email": user,
                "changes":changes
        }
        response = self.put(self.__user_edit_url__, data)
        response = json.loads(response.content)
        if response["status"]!=200:
            raise GetVeroException(response["message"])
         
    def edit_user_tag(self,user,add=[],remove=[]):
        """ see getvero documentation"""
        assert type(add)==list
        assert type(remove)==list
        
        data = {
                "auth_token": self.__aunth_token__,
                "email": user,
                "add":add,
                "remove":remove
        }
        
        response = self.put(self.__user_edit_tag_url__, data)
        response = json.loads(response.content)
        if response["status"]!=200:
            raise GetVeroException(response["message"])
        
    
    def unsubscribe_user(self,user):
        """ see getvero documentation"""
        data = {
            "auth_token": self.__aunth_token__,
            "email": user,
        }
        response = self.post(self.__unsubscribe_user_url__, data)
        response = json.loads(response.content)
        if response["status"]!=200:
            raise GetVeroException(response["message"])
    
    def user_exist(self,user):
        """ check if a user exist on getvero account"""
        try:
            self.edit_user(user)
            return True
        except GetVeroException:
            return False
        
class GetVeroEvents(GetVero):
    """ 
        getvero events api
        https://github.com/getvero/vero-api/blob/master/sections/api/events.md
    """
    __track_url__ = "https://www.getvero.com/api/v2/events/track.json"
    def __init__(self,aunth_token,dev_mode=True):
        '''
        Constructor
        '''
        
        GetVero.__init__(self,aunth_token,dev_mode)
        
    def track_user(self,user,event,data={}):
        """ see getvero documentation"""
        data = {
          "auth_token": self.__aunth_token__,
          "identity": {
            "email": user,
          },
          "event_name": event,
          "data":data
        } 
        response = self.post(self.__track_url__, data)
        response = json.loads(response.content)
        if response["status"]!=200:
            raise GetVeroException(response["message"]) 

