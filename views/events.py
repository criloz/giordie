from db import DB, EmailDB
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import tornado.template
import json

@csrf_exempt
def Events(request):
    """event page"""

    response = HttpResponse("Bad request")

    if request.method=="GET":
        EventHandler.get_argument = request.GET.get
        response = EventHandler.get()
    elif request.method=="POST":
        EventHandler.arguments    = request.POST
        EventHandler.get_argument = request.POST.get
        response = EventHandler.post()
    return response

class EventHandler():
    """
        track  events
    """
    @classmethod
    def get(self):
        #load couchdb
        DB.initialize()

        #obtain a dict with stats of all the events
        events = EmailDB.get_events_count()
        try:
            _file = open("resources/reports/events.html")
            content = _file.read()
            _file.close()
        except:
            return HttpResponse("Internal server error")


        t = tornado.template.Template(content)

        return HttpResponse(t.generate(events= events))

    @classmethod
    def post(self):
        #load couchdb
        DB.initialize()

        uuid = self.get_argument("uuid", None)

        if uuid is None:
            return HttpResponse(json.dumps({"status":400,"message":"Bad request"}))

        event = self.get_argument("event", None)

        if event is None:
            return HttpResponse(json.dumps({"status":400,"message":"Bad request"}))

        #get the email document for this uuid
        email = EmailDB.get_email_by(EmailDB.UUID,include_docs=True,key=uuid)

        if len(email)==0:
            return HttpResponse(json.dumps({"status":400,"message":"Bad request"}))

        for i in email:
            doc = i["doc"]

        #save the event if not exist, if exist add a count
        doc["events"] = doc.get("events",{})
        event_count   = doc["events"].get(event,0)
        doc["events"][event] = event_count + 1
        #update
        EmailDB.update([doc])

        return HttpResponse(json.dumps({"status":200,"message":"event logged"}))