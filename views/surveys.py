from db import DB, EmailDB
from utils import Config
import re
import json
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt

import tornado.template
import time
from utils import DummyObject

def last_dates():

    year   = time.localtime().tm_year
    month  = time.localtime().tm_mon
    months = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    for i in range(12):
        date = DummyObject
        if (month -i) == 0:
            month = month + 12
            year  = year  - 1
        date.nMonth = month -i
        date.sMonth = months[month -i-1]
        date.year   = year
        yield date

@csrf_exempt
def Surveys(request,survey_name):
    """surveys page"""

    response = HttpResponse("Bad request")

    if request.method=="GET":
        SurveysHandler.get_argument = request.GET.get
        response = SurveysHandler.get(survey_name)
    elif request.method=="POST":
        SurveysHandler.arguments    = request.POST
        SurveysHandler.get_argument = request.POST.get
        response = SurveysHandler.post(survey_name)
    return response

class SurveysHandler:

    @classmethod
    def get(self,survey_name):

        #load configuration
        config  = Config()

        #load couchdb
        DB.initialize()

        #load survey
        survey =  DummyObject()
        survey.name = survey_name

        #load getvero settings
        getvero = DummyObject()
        for i in config.getvero:
            getvero.add(i,config.getvero[i])

        uuid = self.get_argument("uuid", None)

        lang = self.get_argument("lang", None)

        if uuid == None:
            return HttpResponse("Bad request")
        try:
            if lang==None:
                _file = open("resources/surveys/"+survey_name+".html")
            else:
                _file = open("resources/surveys/"+survey_name+"_%s.html"%lang)
            content = _file.read()
            _file.close()
        except:
            return HttpResponse("Bad request")

        email = EmailDB.get_email_by(EmailDB.UUID,include_docs=True,key=uuid)

        if len(email)==0:
            return HttpResponse("Bad request")

        transaction = DummyObject()

        for i in email:
            doc = i["doc"]

        #check if the costumer has answer the survey
        surveys = doc.get("surveys",{})
        current_survey = surveys.get(survey_name,None)

        if current_survey!=None:
            return HttpResponse("Already this questionnaire was answered by you")

        for i in doc["transactions"]:
            db_tran = doc["transactions"][i]
            break

        transaction.hotel = db_tran["ORDER INFORMATION"]["Hotel"]
        transaction.uuid  =  uuid

        user = DummyObject()

        user.email       = db_tran["CUSTOMER BILLING INFORMATION"]["E-Mail"]
        user.First_Name  = db_tran["CUSTOMER BILLING INFORMATION"]["First Name"].capitalize()
        user.Last_Name   = db_tran["CUSTOMER BILLING INFORMATION"]["Last Name"].capitalize()

        hotel =   DummyObject()

        #load hotel dict file
        try:
            _file = open("resources/hotels.dataStore","r")
            _dict = json.loads(_file.read())
            _file.close()
        except:
            _dict = {}

        hotel.detail = _dict.get(transaction.hotel,None)
        if hotel.detail!=None:
            d = re.findall("-d([0-9]+)-",hotel.detail)
            if len(d)==1:
                hotel.detail = d[0]

        if hotel.detail==None:
            return HttpResponse("Internal server error, hotel don't exist in the db")

        t = tornado.template.Template(content)

        return HttpResponse(t.generate(user= user,getvero=getvero,transaction=transaction,survey=survey,last_dates=last_dates,hotel=hotel))

    @classmethod
    def post(self,survey_name):

        #load configuration
        config  = Config()

        #load couchdb
        DB.initialize()

        #load getvero
        getvero = DummyObject()
        for i in config.getvero:
            getvero.add(i,config.getvero[i])

        uuid = self.get_argument("uuid", None)
        lang = self.get_argument("lang", None)

        if uuid == None:
            return HttpResponse("Bad request")

        email = EmailDB.get_email_by(EmailDB.UUID,include_docs=True,key=uuid)

        if len(email)==0:
            return HttpResponse("Bad request")

        for i in email:
            doc = i["doc"]

        doc["surveys"]              = doc.get("surveys",{})
        doc["surveys"][survey_name] = doc["surveys"].get(survey_name,{})


        for k in self.arguments:
            if k=="uuid" or k=="survey_name":
                continue
            v = self.arguments[k]

            if (not "comment" in k) and  v=="":
                return HttpResponse("Bad request")

            sub_key = re.findall("%s\[(.*)\]"%survey_name,k)
            if len(sub_key)!=1:
                continue
            doc["surveys"][survey_name][sub_key[0]] = v

        if (len(doc["surveys"][survey_name])<2):
            return HttpResponse("Bad request")

        #update document with the response
        EmailDB.update([doc])

        if survey_name=="survey1":
            if doc["surveys"]["survey1"]["q1"]=="true" and doc["surveys"]["survey1"]["q2"]=="true":
                if lang==None:
                    return redirect("/surveys/survey2?uuid=%s"%uuid)
                else:
                    return redirect("/surveys/survey2?uuid=%s&lang=%s"%(uuid,lang))
        return redirect("/thanks")