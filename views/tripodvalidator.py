from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json
import urllib2

@csrf_exempt
def TripodValidator(request):
    """event page"""

    response = HttpResponse("Bad request")

    if request.method=="GET":
        TripodValidatorHandler.get_argument = request.GET.get
        response = TripodValidatorHandler.get()
    return response

class TripodValidatorHandler():
    @classmethod
    def get(self):

        user = self.get_argument("user", None)

        response = {"answer":False}

        if not (user is None or user==""):

            url = "http://www.tripadvisor.com/members/%s"%user

            try:
                res = urllib2.urlopen(url)
                if res.code!="404":
                    response["answer"]=True
            except:
                pass

        return HttpResponse(json.dumps(response))
