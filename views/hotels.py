from db import DB, EmailDB
import json
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt

import tornado.template
from utils import DummyObject

@csrf_exempt
def Hotels(request):
    """surveys page"""

    response = HttpResponse("Bad request")

    if request.method=="GET":
        DictionaryHandler.get_argument = request.GET.get
        response = DictionaryHandler.get()
    elif request.method=="POST":
        DictionaryHandler.arguments    = request.POST
        DictionaryHandler.get_argument = request.POST.get
        response = DictionaryHandler.post()
    return response

class DictionaryHandler():

    @classmethod
    def get(self):

        #load couchdb
        DB.initialize()

        try:
            _file = open("resources/config/hotel_dictionary.html")
            content = _file.read()
            _file.close()
        except:
            return HttpResponse("Internal server error")

        #get all the hotel on the db
        pre_hotels = EmailDB.get_hotels()
        hotels_set = set()
        for i in pre_hotels:
            hotels_set.add(i["key"])

        hotels = []

        #load hotel dict file
        try:
            _file = open("resources/hotels.dataStore","r")
            _dict = json.loads(_file.read())
            _file.close()
        except:
            _dict = {}


        #create a dummy object for render in the template
        for i in hotels_set:
            new_hotel = DummyObject()
            new_hotel.name = i
            if i in _dict:
                new_hotel.url = _dict[i]
            else:
                new_hotel.url = ""
            hotels.append(new_hotel)

        t = tornado.template.Template(content)

        return HttpResponse(t.generate(hotels= hotels))

    @classmethod
    def post(self):

        _dict = {}

        for k in self.arguments:
            v = self.arguments[k]
            _dict[k] = v

        try:
            _file = open("resources/hotels.dataStore","w+")
            _file.write(json.dumps(_dict))
            _file.close()
        except:
            return HttpResponse("Internal server error")

        return redirect("/hotels/dictionary")
