import imaplib
import logging
import traceback
from co_email.parser import EmailParser
from db import EmailDB

class EmailLogginError(Exception):
    pass

"""
    EmailScrapper class 
"""

class ImapClient():
    
    def __init__(self,server,account,password):
        #create logger
        self.log = logging.getLogger("email."+self.__class__.__name__)
        #load the needed info for get logged into the server
        self.server        = server
        self.user          = account
        self.password      = password
        self.client        = None
        self.parser        = EmailParser.parse_message
        

        #create
 
    def connect(self):
        """
        connect to  imap service
        """
        try:
            mail = imaplib.IMAP4_SSL(self.server)
        except:
            error = traceback.format_exc()
            self.log.error("Could not connect to the server")
            self.log.debug(error)
            raise EmailLogginError("Could not connect to the server")
        try:
            mail.login(self.user, self.password)
        except:
            error = traceback.format_exc()
            self.log.error("Bad user or password")
            self.log.debug(error)
            raise EmailLogginError("Could not connect to the server")
        
        self.client = mail
        self.log.info("connected to the imap server")
        
    def logout(self):
        self.client.close()

    def store_emails(self,search="UNSEEN",flag="\SEEN"):
        """
           process  email, after that, if the operation was successfully mark as a read email
           
           return a list with the emails parsed  
        """
        self.connect()
        self.log.info("Starting to parser unread emails")
        #select inbox
        self.client.select("Inbox",readonly=False)
        #select not read emails   
        
        unseen = self.client.uid('search',None,search)[1][0].split(" ")
        for i in unseen:
            try:
                msg = self.client.fetch(i,"(RFC822)")[1][0][1]
            except:
                self.log.error("Error getting email message")
                self.log.error(traceback.format_exc())
                continue
            #see if the email if a correct email
            msg   = msg.split("**Please DO NOT REPLY to this message. E-mail support@authorize.net if you have any questions.")
            if len(msg)<2:
                #this email does not have the correct email format
                self.log.info("Bad email formated %s"%i)
                continue
            msg = msg[1]
            try:
                #parse email
                doc = self.parser(msg)
                #mark as read
                try:
                    self.client.store(i,'+FLAGS',flag)
                except:
                    self.log.error("Error setting the email as read, email  will be ignored")
                    self.log.error(traceback.format_exc())
                    continue
                doc["message"] = msg
                #save using the key E-Mail as id
                email =  doc["CUSTOMER BILLING INFORMATION"]["E-Mail"].lower().strip()
                if email == "":
                    self.log.warn("skipping item because not have costumer client email.")
                    continue
                hotel =  doc["ORDER INFORMATION"]["Hotel"].lower().replace(" ","")
                _id = hotel + "_" + email
                doc["_id"] = _id
                #save the email
                EmailDB.write(doc, msg)
                
            except:
                self.log.error("Error parsing email")
                self.log.error(traceback.format_exc())
        
        self.log.info("Unread emails parsed")
        self.logout()
        
   
 

